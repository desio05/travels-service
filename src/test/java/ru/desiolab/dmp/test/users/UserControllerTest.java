package ru.desiolab.dmp.test.users;

import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ru.desiolab.dmp.test.locations.LocationDto;
import ru.desiolab.dmp.test.visits.VisitDto;
import ru.desiolab.dmp.test.visits.VisitRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {
    @Mock
    private UserRepository userRepository;

    @Mock
    private VisitRepository visitRepository;

    private UserController userController;
    private UserDto userDto;

    @Before
    public void setUp() throws Exception {
        this.userController = new UserController(userRepository, visitRepository);
        this.userDto = new UserDto(1, "a", "a", 1L, Gender.MALE, "");
        when(userRepository.findOne(userDto.getId())).thenReturn(userDto);
    }

    @Test
    public void getUserVisitsSorting() throws Exception {
        final VisitDto visit1 = new VisitDto(1, 1, null, 1, null, 1000L, 1);
        final VisitDto visit2 = new VisitDto(2, 1, null, 1, null, 1010L, 1);

        when(visitRepository.findAllByUserId(userDto.getId())).thenReturn(Lists.newArrayList(visit2, visit1)); //invalid sorting here

        final List<VisitDto> userVisits = userController.getUserVisits(userDto.getId(), null, null, null, null);
        assertEquals(userVisits, Lists.newArrayList(visit1, visit2));
    }

    @Test
    public void getUserVisitsWithCriteria() throws Exception {
        final LocationDto location1 = new LocationDto(1, "Russia", "Moscow", "Galereya", 100);
        final LocationDto location2 = new LocationDto(2, "Russia", "Moscow", "Kremlin", 10);
        final LocationDto location3 = new LocationDto(3, "USA", "New Yourk", "Time Square", 1);
        final VisitDto visit1 = new VisitDto(1, 1, null, 1, location1, 1000L, 1);
        final VisitDto visit2 = new VisitDto(2, 1, null, 1, location2, 600L, 1);
        final VisitDto visit3 = new VisitDto(3, 1, null, 1, location2, 1010L, 1);
        final VisitDto visit4 = new VisitDto(4, 1, null, 1, location3, 1000L, 1);

        when(visitRepository.findAllByUserId(userDto.getId())).thenReturn(Lists.newArrayList(visit1, visit2, visit3, visit4));

        final List<VisitDto> userVisits = userController.getUserVisits(userDto.getId(), 500L, 1010L, "Russia", 50);
        assertEquals(Lists.newArrayList(visit2), userVisits);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getUserVisitsUserNotFound() throws Exception {
        userController.getUserVisits(123, null, null, null, null);
    }
}