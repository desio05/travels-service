package ru.desiolab.dmp.test.users;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import ru.desiolab.dmp.test.TestSecurityConfiguration;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Import(TestSecurityConfiguration.class)
@Transactional
@ActiveProfiles("test")
public class UserControllerIntegrationTest {

    @Inject
    private TestRestTemplate restTemplate;

    @Inject
    private EntityManager entityManager;

    private UserDto userDto;

    @Before
    public void setUp() throws Exception {
        this.userDto = new UserDto(null, "a", "a", 0L, Gender.MALE, "a@a.com");
        entityManager.persist(userDto);
        entityManager.createNativeQuery("SET DATABASE TRANSACTION CONTROL MVCC").executeUpdate();
        this.restTemplate = restTemplate.withBasicAuth("test", "test");
    }

    @Test
    public void getUserById() throws Exception {
        final UserDto user = restTemplate.getForEntity("/users/" + userDto.getId(), UserDto.class).getBody();
        assertEquals(userDto, user);
    }

    @Test
    public void createUser() throws Exception {
        UserDto createdUser = new UserDto(null, "b", "b", 10L, Gender.FEMALE, "b@b.com");
        final UserDto createdUserResponse = restTemplate.postForEntity("/users/new", createdUser, UserDto.class).getBody();
        createdUser.setId(createdUserResponse.getId());
        assertEquals(createdUser, createdUserResponse);
    }

    @Test
    public void updateUser() throws Exception {
        UserDto createdUser = new UserDto(null, "b", "b", 10L, Gender.FEMALE, "b@b.com");
        final UserDto createdUserResponse = restTemplate.postForEntity("/users/new", createdUser, UserDto.class).getBody();
        createdUser.setId(createdUserResponse.getId());

        final JsonNodeFactory nodeFactory = new ObjectMapper().getNodeFactory();
        final ObjectNode updateUserRequest = nodeFactory.objectNode();
        updateUserRequest.set("email", nodeFactory.textNode("bb@bb.com"));
        final ResponseEntity<UserDto> userDtoResponseEntity = restTemplate.exchange("/users/" + createdUser.getId(), HttpMethod.PUT, new HttpEntity<>(updateUserRequest), UserDto.class);
        final UserDto updatedUser = userDtoResponseEntity.getBody();

        createdUser.setEmail("bb@bb.com");

        assertEquals(updatedUser, createdUser);
    }
}
