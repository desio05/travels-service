package ru.desiolab.dmp.test;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import javax.inject.Inject;

@Configuration
@Order(1000)
public class TestSecurityConfiguration extends WebSecurityConfigurerAdapter {
    public static final String USER = "USER";

    @Inject
    public void configureAuthenticationStore(AuthenticationManagerBuilder builder) throws Exception {
        builder.inMemoryAuthentication()
                .withUser("test").password("test").roles(USER);
    }
}
