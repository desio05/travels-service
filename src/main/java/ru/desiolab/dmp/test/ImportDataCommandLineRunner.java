package ru.desiolab.dmp.test;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import ru.desiolab.dmp.test.locations.LocationDto;
import ru.desiolab.dmp.test.locations.LocationRepository;
import ru.desiolab.dmp.test.users.UserDto;
import ru.desiolab.dmp.test.users.UserRepository;
import ru.desiolab.dmp.test.visits.VisitDto;
import ru.desiolab.dmp.test.visits.VisitRepository;

import javax.inject.Inject;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Profile("!test")
public class ImportDataCommandLineRunner implements CommandLineRunner {

    private static final Logger LOG = LoggerFactory.getLogger(TravelsApplication.class);

    private final LocationRepository locationRepository;
    private final UserRepository userRepository;
    private final VisitRepository visitRepository;

    @Inject
    public ImportDataCommandLineRunner(LocationRepository locationRepository,
                                       UserRepository userRepository,
                                       VisitRepository visitRepository) {
        this.locationRepository = locationRepository;
        this.userRepository = userRepository;
        this.visitRepository = visitRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        final TypeFactory typeFactory = objectMapper.getTypeFactory();
        final JavaType stringType = typeFactory.constructType(String.class);

        LOG.info("Importing data into database...");
        LOG.info("Importing locations into database...");
        File locationsJson = new ClassPathResource("data/locations_1.json").getFile();
        final CollectionType locationListType = typeFactory.constructCollectionType(List.class, LocationDto.class);
        final MapType locationInputObjectType = objectMapper.getTypeFactory().constructMapType(Map.class, stringType, locationListType);
        final Map<String, List<LocationDto>> locationsRawObject = objectMapper.readValue(locationsJson, locationInputObjectType);
        locationRepository.save(locationsRawObject.get("locations"));

        LOG.info("Importing users into database...");
        File usersJson = new ClassPathResource("data/users_1.json").getFile();
        final CollectionType userListType = typeFactory.constructCollectionType(List.class, UserDto.class);
        final MapType userInputObjectType = objectMapper.getTypeFactory().constructMapType(HashMap.class, stringType, userListType);
        final Map<String, List<UserDto>> usersRawObject = objectMapper.readValue(usersJson, userInputObjectType);
        userRepository.save(usersRawObject.get("users"));

        LOG.info("Importing visits into database...");
        File visitsJson = new ClassPathResource("data/visits_1.json").getFile();
        final CollectionType visitListType = typeFactory.constructCollectionType(List.class, VisitDto.class);
        final MapType visitInputObjectType = objectMapper.getTypeFactory().constructMapType(HashMap.class, stringType, visitListType);
        final Map<String, List<VisitDto>> visitsRawObject = objectMapper.readValue(visitsJson, visitInputObjectType);
        visitRepository.save(visitsRawObject.get("visits"));

        LOG.info("Importing finished successfully");
    }
}
