package ru.desiolab.dmp.test.security;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.inject.Inject;

@EnableWebSecurity
public class SimpleSecurityConfig extends WebSecurityConfigurerAdapter {
    public static final String USER = "USER";

    private final AuthenticationEntryPoint authEntryPoint;

    @Inject
    public SimpleSecurityConfig(AuthenticationEntryPoint authEntryPoint) {
        this.authEntryPoint = authEntryPoint;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests().anyRequest().authenticated()
                .and().httpBasic().authenticationEntryPoint(authEntryPoint);
    }

    @Inject
    public void configureAuthenticationStore(AuthenticationManagerBuilder builder) throws Exception {
        builder.inMemoryAuthentication()
                .withUser("user").password("user").roles(USER);
    }
}
