package ru.desiolab.dmp.test.visits;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.desiolab.dmp.test.locations.LocationDto;
import ru.desiolab.dmp.test.users.UserDto;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "visits")
@Entity
public class VisitDto {
    @Id
    @GeneratedValue
    private Integer id;

    @JsonProperty("user")
    private Integer userId;

    @JoinColumn(name = "userId", insertable = false, updatable = false)
    @ManyToOne
    @JsonIgnore
    private UserDto userDto;

    @JsonProperty("location")
    private Integer locationId;

    @JoinColumn(name = "locationId", insertable = false, updatable = false)
    @ManyToOne
    @JsonIgnore
    private LocationDto locationDto;

    @JsonProperty("visited_at")
    private Long visitedAt;

    private Integer mark;
}
