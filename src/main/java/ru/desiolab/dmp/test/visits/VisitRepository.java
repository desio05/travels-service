package ru.desiolab.dmp.test.visits;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface VisitRepository extends CrudRepository<VisitDto, Integer> {
    List<VisitDto> findAllByUserId(Integer userId);

    List<VisitDto> findAllByLocationId(Integer id);
}
