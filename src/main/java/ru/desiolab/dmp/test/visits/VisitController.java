package ru.desiolab.dmp.test.visits;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
public class VisitController {
    private final VisitRepository visitRepository;

    @Inject
    public VisitController(VisitRepository visitRepository) {
        this.visitRepository = visitRepository;
    }

    @RequestMapping(path = "/visits/{id}", method = RequestMethod.GET)
    @Cacheable(value = "visits", key = "#id")
    public VisitDto getVisit(@PathVariable("id") Integer id) {
        final VisitDto visit = visitRepository.findOne(id);
        if (visit == null) {
            throw new EntityNotFoundException("Can't get visit. Visit with id=" + id + " has not been found");
        }
        return visit;
    }

    @RequestMapping(path = "/visits/new", method = RequestMethod.POST)
    @CacheEvict(value = "locations_marks", key = "#visit.locationId")
    public VisitDto createVisit(@RequestBody VisitDto visit) {
        return visitRepository.save(visit);
    }

    @RequestMapping(path = "/visits/{id}", method = RequestMethod.PUT)
    @CacheEvict(value = "locations_marks", allEntries = true)
    @CachePut(value = "visits", key = "#id")
    public VisitDto updateVisit(@PathVariable("id") Integer id, HttpServletRequest request) throws IOException {
        final VisitDto visit = visitRepository.findOne(id);
        if (visit == null) {
            throw new EntityNotFoundException("Can't update visit. Visit with id=" + id + " has not been found");
        }
        final ObjectMapper objectMapper = new ObjectMapper();
        try {
            VisitDto updatedVisit = objectMapper.readerForUpdating(visit).readValue(request.getReader());
            return visitRepository.save(updatedVisit);
        } catch (Exception e) {
            throw new IllegalArgumentException("Wrong request body", e.getCause());
        }
    }
}
