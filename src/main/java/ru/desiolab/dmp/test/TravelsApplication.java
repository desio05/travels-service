package ru.desiolab.dmp.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class TravelsApplication {
    public static void main(String[] args) {
        SpringApplication.run(TravelsApplication.class, args);
    }
}
