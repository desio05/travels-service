package ru.desiolab.dmp.test.locations;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "locations")
@Entity
public class LocationDto {
    @Id
    @GeneratedValue
    private Integer id;

    private String country;
    private String city;
    private String place;
    private Integer distance;
}
