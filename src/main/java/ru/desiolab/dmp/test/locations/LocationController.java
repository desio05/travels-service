package ru.desiolab.dmp.test.locations;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;
import ru.desiolab.dmp.test.users.Gender;
import ru.desiolab.dmp.test.visits.VisitDto;
import ru.desiolab.dmp.test.visits.VisitRepository;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.OptionalDouble;

@RestController
public class LocationController {

    private static final Logger LOG = LoggerFactory.getLogger(LocationController.class);

    private final LocationRepository locationRepository;
    private final VisitRepository visitRepository;

    @Inject
    public LocationController(LocationRepository locationRepository, VisitRepository visitRepository) {
        this.locationRepository = locationRepository;
        this.visitRepository = visitRepository;
    }

    @RequestMapping(path = "/locations/{id}", method = RequestMethod.GET)
    @Cacheable(value = "locations", key = "#id")
    public LocationDto getLocation(@PathVariable("id") Integer id) {
        final LocationDto location = locationRepository.findById(id);
        if (location == null) {
            throw new EntityNotFoundException("Can't get location. Location with id=" + id + " has not been found");
        }
        return location;
    }

    @RequestMapping(path = "/locations/new", method = RequestMethod.POST)
    @CacheEvict(value = "locations_marks", allEntries = true)
    public LocationDto createLocation(@RequestBody LocationDto location) {
        return locationRepository.save(location);
    }

    @RequestMapping(path = "/locations/{id}", method = RequestMethod.PUT)
    @CachePut(value = "locations", key = "#id")
    public LocationDto updateLocation(@PathVariable("id") Integer id, HttpServletRequest request) throws IOException {
        final LocationDto location = locationRepository.findById(id);
        if (location == null) {
            throw new EntityNotFoundException("Can't update location. Location with id=" + id + " has not been found");
        }
        final ObjectMapper objectMapper = new ObjectMapper();
        try {
            LocationDto updatedLocation = objectMapper.readerForUpdating(location).readValue(request.getReader());
            return locationRepository.save(updatedLocation);
        } catch (Exception e) {
            throw new IllegalArgumentException("Wrong request body", e.getCause());
        }
    }

    @RequestMapping(path = "/locations/{id}/avg", method = RequestMethod.GET)
    @Cacheable(value = "locations_marks", key = "#id")
    public Double calculateAverageMark(@PathVariable("id") Integer id,
                                       @RequestParam(value = "fromDate", required = false) Long fromDate,
                                       @RequestParam(value = "toDate", required = false) Long toDate,
                                       @RequestParam(value = "fromAge", required = false) Long fromAge,
                                       @RequestParam(value = "toAge", required = false) Long toAge,
                                       @RequestParam(value = "gender", required = false) Gender gender) {
        LOG.info("Recalculating avg");
        OptionalDouble averageOptional = visitRepository.findAllByLocationId(id)
                .parallelStream().filter(visitDto -> {
                    boolean accept = true;
                    if (fromDate != null) {
                        accept = visitDto.getVisitedAt() > fromDate;
                    }
                    if (toDate != null) {
                        accept &= visitDto.getVisitedAt() < toDate;
                    }
                    if (fromAge != null || toAge != null) {
                        Instant birthDate = Instant.ofEpochSecond(visitDto.getUserDto().getBirthDate());
                        LocalDate birthDateLocalDate = birthDate.atZone(ZoneId.systemDefault()).toLocalDate();
                        Integer age = Period.between(birthDateLocalDate, LocalDate.now()).getYears();
                        if (fromAge != null) {
                            accept &= age > fromAge;
                        }
                        if (toAge != null) {
                            accept &= age < toAge;
                        }
                    }
                    if (gender != null) {
                        accept &= visitDto.getUserDto().getGender() == gender;
                    }
                    return accept;
                }).mapToInt(VisitDto::getMark).average();
        return averageOptional.orElse(0.0d);
    }
}
