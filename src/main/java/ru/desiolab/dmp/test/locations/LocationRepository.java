package ru.desiolab.dmp.test.locations;

import org.springframework.data.repository.CrudRepository;

public interface LocationRepository extends CrudRepository<LocationDto, Integer> {
    LocationDto findById(Integer id);
}
