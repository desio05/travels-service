package ru.desiolab.dmp.test.error;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class GeneralErrorHandler {

    private static final Logger LOG = LoggerFactory.getLogger(GeneralErrorHandler.class);

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {IllegalArgumentException.class, MethodArgumentTypeMismatchException.class})
    protected void handleBadRequestException(RuntimeException e, HttpServletRequest request, HttpServletResponse response) throws IOException {
        fillResponse(HttpStatus.BAD_REQUEST, e, request, response);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = EntityNotFoundException.class)
    protected void handleNotFoundException(EntityNotFoundException e, HttpServletRequest request, HttpServletResponse response) throws IOException {
        fillResponse(HttpStatus.NOT_FOUND, e, request, response);
    }

    private void fillResponse(HttpStatus status, RuntimeException e, HttpServletRequest request, HttpServletResponse response) throws IOException {
        Map<String, String> body = new HashMap<String, String>();
        body.put("status", status.toString());
        body.put("error", status.getReasonPhrase());
        body.put("exception", e.getClass().getName());
        body.put("message", e.getMessage());
        body.put("path", request.getServletPath());
        final String jsonResponse = new Gson().toJson(body);
        response.setStatus(status.value());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.getWriter().write(jsonResponse);
        response.getWriter().flush();
    }
}
