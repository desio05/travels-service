package ru.desiolab.dmp.test;

import com.hazelcast.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import ru.desiolab.dmp.test.users.GenderConverter;

@Configuration
public class GlobalConfiguration extends WebMvcConfigurationSupport {
    @Override
    public FormattingConversionService mvcConversionService() {
        final FormattingConversionService formattingConversionService = super.mvcConversionService();
        formattingConversionService.addConverter(new GenderConverter());
        return formattingConversionService;
    }

    @Bean
    public Config config() {
        return new Config();
    }
}
