package ru.desiolab.dmp.test.users;


import org.springframework.core.convert.converter.Converter;

public class GenderConverter implements Converter<String, Gender> {
    @Override
    public Gender convert(String source) {
        final Gender gender = Gender.fromString(source);
        if (gender == null) {
            throw new IllegalArgumentException("Gender with value=" + source + " is unsupported");
        }
        return gender;
    }
}
