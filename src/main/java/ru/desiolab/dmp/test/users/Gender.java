package ru.desiolab.dmp.test.users;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Gender {
    @JsonProperty("m")
    MALE("m"),

    @JsonProperty("f")
    FEMALE("f");

    private final String code;

    Gender(String code) {
        this.code = code;
    }

    public static Gender fromString(String source) {
        for (Gender gender : Gender.values()) {
            if (gender.code.equals(source)) {
                return gender;
            }
        }
        return null;
    }
}
