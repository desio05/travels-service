package ru.desiolab.dmp.test.users;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserDto, Integer> {
}
