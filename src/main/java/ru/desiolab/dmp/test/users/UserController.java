package ru.desiolab.dmp.test.users;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;
import ru.desiolab.dmp.test.visits.VisitDto;
import ru.desiolab.dmp.test.visits.VisitRepository;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class UserController {
    private final UserRepository userRepository;
    private final VisitRepository visitRepository;

    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

    @Inject
    public UserController(UserRepository userRepository,
                          VisitRepository visitRepository) {
        this.userRepository = userRepository;
        this.visitRepository = visitRepository;
    }

    @RequestMapping(path = "/users/{id}", method = RequestMethod.GET)
    @Cacheable(value = "users", key = "#id")
    public UserDto getUser(@PathVariable("id") Integer id) {
        LOG.info("Accessing the DB for user");
        final UserDto user = userRepository.findOne(id);
        if (user == null) {
            throw new EntityNotFoundException("Can't get user. User with id=" + id + " has not been found");
        }
        return user;
    }

    @RequestMapping(path = "/users/new", method = RequestMethod.POST)
    public UserDto createUser(@RequestBody UserDto user) {
        return userRepository.save(user);
    }

    @RequestMapping(path = "/users/{id}", method = RequestMethod.PUT)
    @CacheEvict(value = "users", key = "#id")
    public UserDto updateUser(@PathVariable("id") Integer id, HttpServletRequest request) throws IOException {
        final UserDto user = userRepository.findOne(id);
        if (user == null) {
            throw new EntityNotFoundException("Can't update user. User with id=" + id + " has not been found");
        }
        final ObjectMapper objectMapper = new ObjectMapper();
        try {
            UserDto updatedUser = objectMapper.readerForUpdating(user).readValue(request.getReader());
            return userRepository.save(updatedUser);
        } catch (Exception e) {
            throw new IllegalArgumentException("Wrong request body", e.getCause());
        }
    }

    @RequestMapping(path = "/users/{id}/visits", method = RequestMethod.GET)
    @Cacheable("users")
    public List<VisitDto> getUserVisits(@PathVariable("id") Integer id,
                                        @RequestParam(name = "fromDate", required = false) Long fromDate,
                                        @RequestParam(name = "toDate", required = false) Long toDate,
                                        @RequestParam(name = "country", required = false) String country,
                                        @RequestParam(name = "toDistance", required = false) Integer toDistance) {
        final UserDto user = userRepository.findOne(id);
        if (user == null) {
            throw new EntityNotFoundException("Can't get user's visits. User with id=" + id + " has not been found");
        }
        return visitRepository.findAllByUserId(id)
                .parallelStream()
                .filter((visitDto) -> {
                    boolean accept = true;
                    if (fromDate != null) {
                        accept = visitDto.getVisitedAt() > fromDate;
                    }
                    if (toDate != null) {
                        accept &= visitDto.getVisitedAt() < toDate;
                    }
                    if (country != null) {
                        accept &= visitDto.getLocationDto().getCountry().equals(country);
                    }
                    if (toDistance != null) {
                        accept &= visitDto.getLocationDto().getDistance() < toDistance;
                    }
                    return accept;
                })
                .sorted(Comparator.comparing(VisitDto::getVisitedAt))
                .collect(Collectors.toList());
    }
}
